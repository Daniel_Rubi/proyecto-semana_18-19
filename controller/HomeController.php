<?php
class HomeController
{
    public function __construct()
    {
        require_once '././views/header.php';
        view::load('home');
    }
}
