<?php
class CarreraController extends CrudModel
{
    private $carrera;

    public function __construct()
    {
        $this->carrera = new CarreraModel();
    }

    public function  listar()
    {
        $carreras  = $this->carrera->listar();
        require_once '././views/header.php';
        view::load('carrera/listar', array("carreras" => $carreras));
    }
    public function Crud()
    {
        if (isset($_REQUEST["id"])) {
            $codigo = $this->carrera->setCodigo($_REQUEST["id"]);
            $this->carrera->getbyCod($codigo);
        }
        require_once "././views/carrera/form.php";
    }
    public function guardar()
    {
        if (empty(trim($_REQUEST["carrera"])) || empty(trim($_REQUEST["titulo"]))) {
            echo "<script>alert('Todos los datos son obligatorios'); window.close(); window.location= 'index.php?url=carrera&action=listar'</Script>";
        } else {
            $this->carrera->setCodigo($_REQUEST["codCarrera"]);
            $this->carrera->setNombre(utf8_decode(utf8_encode(strip_tags($_REQUEST["carrera"]))));
            $this->carrera->setTitulo(utf8_decode(utf8_encode(strip_tags($_REQUEST["titulo"]))));
            $this->carrera->setCodFacultad($_REQUEST["facultad"]);

            if ($_REQUEST["codCarrera"] == 0) {
                $this->carrera->guardar();
                echo "<script>alert('Registro guardado correctamente'); opener.location.reload(); window.close();</Script>";
            } else {
                $this->carrera->editar();
                echo "<script>alert('Registro editado correctamente'); opener.location.reload(); window.close();</Script>";
            }
        }
    }

    public function eliminar()
    {
        $this->carrera->setCodigo($_REQUEST["id"]);
        $this->carrera->eliminar();
        header('Location: index.php?url=carrera&action=listar');
    }
}
