<?php

class Router
{
    public function __construct()
    {
        if (!isset($_GET['url']) || empty($_GET['url'])) {
            $home  = new HomeController();
        } else {
            $controller_name = ucwords($_GET['url']) . "Controller";

            if (!isset($_GET['action']) || empty($_GET['action'])) {
                $action_name = 'Listar';
            } else {
                $action_name = ucwords($_GET['action']);
            }

            if (class_exists($controller_name)) {
                $classController = new $controller_name;
                if (method_exists($classController, $action_name)) {
                    $classController->$action_name();
                } else {
                    View::load('404', array("error" => "funcion", "causa" => $action_name));
                }
            } else {
                View::load('404', array("error" => "controlador", "causa" => $_GET['url']));
            }
        }
    }
}
