<?php
class CarreraModel extends CrudModel
{
    private $codigo;
    private $nombre;
    private $titulo;
    private $codFacultad;


    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }
    public function getCodigo()
    {
        return $this->codigo;
    }
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }
    public function getNombre()
    {
        return $this->nombre;
    }
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }
    public function getTitulo()
    {
        return $this->titulo;
    }
    public function setCodFacultad($codFacultad)
    {
        $this->codFacultad = $codFacultad;
    }
    public function getCodFacultad()
    {
        return $this->codFacultad;
    }

    public function listar()
    {
        $this->sql = "SELECT * FROM carrera";
        return parent::getAll();
    }
    //insertar registros
    public function guardar()
    {
        $this->sql = "INSERT INTO carrera
        (
            nombreCarrera,
            titulo,
            codFacultad
        )
        VALUES
        (
            :nombre,
            :titulo,
            :codFacultad
        )";
        $vals = array(
            ":nombre" => $this->nombre,
            ":titulo" => $this->titulo,
            ":codFacultad" => $this->codFacultad
        );
        return parent::save($vals);
    }
    //editar registros
    public function editar()
    {
        $this->sql = "UPDATE carrera SET
        nombreCarrera=:nombre,
        titulo=:titulo,
        codFacultad=:codFacultad
        WHERE codCarrera=:codigo";

        $vals = array(
            ":codigo" => $this->codigo,
            ":nombre" => $this->nombre,
            ":titulo" => $this->titulo,
            ":codFacultad" => $this->codFacultad
        );
        return parent::edit($vals);
    }
    //registros por codigo
    public function getbyCod()
    {
        $this->sql = "SELECT * FROM carrera WHERE codCarrera=:codCarrera";
        $id = array(
            ":codCarrera" => $this->codigo
        );
        return parent::getbyID($id);
    }

    //eliminar registros
    public function eliminar()
    {
        $this->sql = "DELETE FROM carrera WHERE codCarrera=:codigo";
        $id = array(
            ":codigo" => $this->codigo
        );
        return parent::deleteByID($id);
    }
}
