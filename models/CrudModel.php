<?php
class CrudModel
{
    public $sql;
    public $db;

    public function __construct()
    {
        require_once "ConexionModel.php";
        $pdo = new ConexionModel();
        $this->db = $pdo;
    }

    public function getAll(){
        try{
            $stmt= $this->db->dbh->prepare($this->sql);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();

            $rows= $stmt->fetchAll();
            return $rows;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
    public function save($values){
        try{
            $stmt=$this->db->dbh->prepare($this->sql);
            $stmt->execute($values);
        }catch(PDOException $e){
            echo $e->getMessage();
        }

    }
    public function edit($values){
        try{
            $stmt=$this->db->dbh->prepare($this->sql);
            $stmt->execute($values);
        }catch(PDOException $e){
                echo $e->getMessage();
        }
    }
    public function deleteByID($id){
        try{
            $stmt= $this->db->dbh->prepare($this->sql);
            $stmt->execute($id);
        }catch(PDOException $e){
            echo $e->getMessage();
        }

    }
    public function getbyID($id){
        try{
            $stmt=$this->db->dbh->prepare($this->sql);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute($id);
            $rows= $stmt->fetchAll();
            return $rows;
        }catch(PDOException $e){
            $e->getMessage();

        }
    }
}
