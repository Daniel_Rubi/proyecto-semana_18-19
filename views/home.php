
<div class="integrantes">
    <h3 class="text-center">Ingeniería en sistemas y redes.</h3>
    <ul>
        <li><strong>Marcos Daniel Rubí Hernández</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SMIS925620
            <ul>
                <li>MVC es un patrón de diseño que nos ayuda a separar las tareas que realiza nuestro código, facilitando así el encontrar errores y poder realizar pruebas de manera individual.</li>
            </ul>
        </li>
        <li><strong>Gerson Usiel Quintanilla Sánchez</strong> &nbsp;&nbsp;SMIS004718
            <ul>
                <li>MVC nos sirve para darle una estructura mas organizada a nuestros proyectos, esto a su vez nos ayuda a dividir la lógica en 3 componentes. <br>
                    Modelo = todo lo relacionado a la base de datos.<br>
                    Vista = Los diseños que ve el usuario. <br>
                    Controlador = Sirve de puente entre el modelo y la vista.    
                </li>
            </ul>
        </li>
    </ul>
</div>
</body>

</html>