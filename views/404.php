<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>404 - Proyecto MVC</title>
    <link rel="stylesheet" href="public/css/404.css">
</head>

<body>
    <div class="content">
        <?php 
        if (strcmp($error, 'funcion') === 0) {
            echo "<h1>No se encontro o no existe la función <span>$causa</span></h1>";
        }
        if (strcmp($error, 'controlador') === 0) {
            echo "<h1>No se encontro o no existe el controlador <span>$causa</span></h1>";
        } ?>

        <p>Será llevado al inicio en: <span id="contador"></span></p>
        <a href="index.php">Ir a inicio</a>
        <img src="public/img/img_404.png" alt="">
    </div>
    <script src="public/js/scripts.js"></script>
</body>

</html>