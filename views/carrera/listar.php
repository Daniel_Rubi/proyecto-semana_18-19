<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Carreras</title>
    <!-- BOOTSTRAP 4.4.1 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- FONT AWESOME -->
    <script src="https://kit.fontawesome.com/87b8bff04b.js" crossorigin="anonymous"></script>
    <!-- STYLES -->
    <link rel="stylesheet" href="public/css/listar.css">
</head>

<body>

    <div class="opciones text-center p-4">
        <a onclick="javascript:formNuevo();" class="bg-primary">Nueva carrera</a>
        <a href="index.php" class="bg-danger">Cerrar</a>
    </div>
    <div class="row d-flex justify-content-center col-md-12">
        <table class="table table-striped table-bordered col-md-11 table-sm bg-light">
            <thead class="bg-info text-white">
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Titulo</th>
                    <th scope="col">Facultad</th>
                    <th scope="col" colspan="2"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($carreras as $key => $carrera): ?>
                    <tr>
                        <td><?= utf8_decode(utf8_encode($carrera['nombreCarrera'])); ?></td>
                        <td><?= utf8_decode(utf8_encode($carrera['titulo'])); ?></td>
                        <?php
                        if ($carrera['codFacultad'] == 1) {
                            echo "<td>Facultad de informática</td>";
                        }else{
                            echo "<td>Facultad de ciencias empresariales</td>";
                        }
                        $id = $carrera['codCarrera'];
                        ?>
                        <td class='icon'><a onclick='javascript:formEditar(<?php echo $id?>);' title='Editar'><i class='fa fa-edit'></i></a></td>
                        <td class='icon'><a onclick='javascript:borrar(<?php echo $id;?>);' title='Eliminar'><i class='fa fa-trash-alt red'></i></a></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <script type="text/javascript">
        function borrar(id) {
            var resp = confirm("Seguro que desea eliminar el registro?");
            if (resp == true) {
                window.location.href = "index.php?url=carrera&action=eliminar&id="+id+"";
            }
        }

        function formNuevo() {
            window.open('index.php?url=carrera&action=crud', 'frmAgregar', 'width=600,height=600');

        }

        function formEditar(id) {
            window.open('index.php?url=carrera&action=crud&id='+id, 'frmEditar', 'width=600,height=600');
        }
    </script>

</body>


</html>