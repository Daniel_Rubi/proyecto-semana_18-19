<h1 class="text-center text-primary">
<?php
    $data= $this->carrera->getbyCod();

    if(isset($_REQUEST["id"]) != null){
        echo "Editar";
        $accion="Editar";
        $codigo= $data[0]["codCarrera"];
        $nombre= utf8_decode(utf8_encode($data[0]["nombreCarrera"]));
        $titulo= utf8_decode(utf8_encode($data[0]["titulo"]));
        $codFacultad= $data[0]["codFacultad"];
    }else{
        echo "Nueva Carrera";
        $accion= "Guardar";
        $codigo=0;
        $nombre="";
        $titulo="";
        $codFacultad=0;
    }

?>
</h1>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Agregar Carrera</title>
    <!-- BOOTSTRAP 4.4.1 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>

<body>
    <div class="container">
        <form class="col-md-8" action="index.php?url=carrera&action=guardar" method="POST">
            <input type="hidden" name="codCarrera" value="<?=$codigo?>">
            <div class="form-group">
                <label for="carrera">Carrera</label>
                <input type="text" class="form-control" id="carrera"  name="carrera" value="<?=$nombre?>">
            </div>
            <div class="form-group">
                <label for="titulo">Título</label>
                <input type="text" class="form-control" id="titulo" name="titulo"  value="<?=$titulo?>">
            </div>
            <div class="form-group">
                <label for="facultad">Facultad</label>
                <select id="facultad" class="form-control" name="facultad"  value="<?=$codFacultad?>">
                    <option value="1" <?php if($codFacultad==1){echo "selected";}?>>Facultad de informática</option>
                    <option value="2" <?php if($codFacultad==2){echo "selected";}?>>Facultad de ciencias empresariales</option>
                </select>
            </div>
            <input type="submit" class="btn btn-primary" name="submit" value="<?=$accion?>"></input>
            <button type="reset" onclick="javascript:cerrar();" class="btn btn-secondary">Cancelar</button>
        </form>
    </div>
    <script type="text/javascript">
        function cerrar(){
            window.close();
        }
    </script>
</body>

</html>