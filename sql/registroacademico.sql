-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-05-2020 a las 19:32:21
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `registroacademico`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE `alumno` (
  `codAlumno` int(11) NOT NULL,
  `nombreCompleto` varchar(100) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `tipoDocumento` int(11) DEFAULT NULL,
  `numeroDocumento` varchar(15) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `sexo` varchar(10) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `correo` varchar(25) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `numeroTelefono` varchar(15) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `direccion` varchar(100) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `codCarrera` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`codAlumno`, `nombreCompleto`, `tipoDocumento`, `numeroDocumento`, `sexo`, `correo`, `numeroTelefono`, `direccion`, `codCarrera`) VALUES
(1, 'Juan Alberto Flores Morales', 1, '043589939-2', 'Masculino', 'juan@gmail.com', '7582-6943', 'Usulután', 1),
(2, 'Alejandra Sofia Silva Canizalez', 1, '095539939-2', 'Femenino', 'asilva@gmail.com', '7292-5789', 'San Miguel', 1),
(3, 'Maria de los Angeles Gonzalez Flores', 1, '092223579-2', 'Femenino', 'mariaG@gmail.com', '7500-7543', 'San Miguel', 2),
(4, 'Jose Alberto Castillo Flores', 1, '087899639-2', 'Masculino', 'albcastillo@gmail.com', '7550-5689', 'San Miguel', 2),
(5, 'Dina Argueta Molina', 1, '022553669-2', 'Femenino', 'dina2142@gmail.com', '7382-5963', 'San Miguel', 3),
(6, 'Wilfredo Antonio Ganuza Portillo', 1, '075896347-3', 'Masculino', 'wil.ganuza@gmail.com', '7222-8999', 'San Miguel', 3),
(7, 'Carmen Lourdes Linares Lazo', 1, '089735439-6', 'Femenino', 'clourdes@gmail.com', '2645-8963', 'San Miguel', 4),
(8, 'Luis Jose Ventura Ventura', 1, '087412369-8', 'Masculino', 'jventura@gmail.com', '2296-3545', 'San Miguel', 5),
(9, 'Claudia Estela Villalobos Ventura', 1, '089754398-4', 'Femenino', 'cl214@gmail.com', '7789-4152', 'Usulután', 7),
(10, 'Martin Benitez Benitez', 1, '054789357-2', 'Masculino', 'mb2812@gmail.com', '2789-6389', 'San Miguel', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrera`
--

CREATE TABLE `carrera` (
  `codCarrera` int(11) NOT NULL,
  `nombreCarrera` varchar(50) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `titulo` varchar(50) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `codFacultad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `carrera`
--

INSERT INTO `carrera` (`codCarrera`, `nombreCarrera`, `titulo`, `codFacultad`) VALUES
(1, 'Ingeniería en Sistemas', 'Ingeniero en Sistemas', 1),
(2, 'Técnico en Sistemas', 'Técnico en Sistemas', 1),
(3, 'Administración de Redes', 'Adminsitrador de Redes de Computadoras', 1),
(4, 'Administración de Bases de Datos', 'Administrador de Bases de Datos', 1),
(5, 'Tecnico en Administración de Redes', 'Técnico Administrador de Redes', 1),
(6, 'Licenciatura en Administración de Empresas', 'Licenciado en Administración de Empresas', 2),
(7, 'Licenciatura en Contaduría Pública', 'Licenciado en Contaduría Pública', 2),
(8, 'Técnico en Contaduría Pública', 'Técnico en Contaduría Pública', 2),
(9, 'Licenciatura en Mercadeo', 'Licenciado en Mercadeo', 2),
(10, 'Técnico en Mercadeo', 'Técnico en Mercadeo', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docente`
--

CREATE TABLE `docente` (
  `codDocente` int(11) NOT NULL,
  `nombreDocente` varchar(255) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `correo` varchar(100) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `numeroTelefono` varchar(10) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `direccion` varchar(30) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `codCarrera` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `docente`
--

INSERT INTO `docente` (`codDocente`, `nombreDocente`, `correo`, `numeroTelefono`, `direccion`, `codCarrera`) VALUES
(1, 'Lydie Argueta Delgado', 'VelaDelgado@gmail.com', '7027-3746', 'San Miguel', 1),
(2, 'Udolfo Rael Tejeda', 'RaelTejeda@gmail.com', '8689-2431', 'San Salvador', 2),
(3, 'Mael Pichardo Meraz', 'MaelPichera@gmail.com', '7364-4574', 'San Miguel', 3),
(4, 'Atila Manzano Espinoza', 'Espinoza@gmail.com', '7364-3447', 'San Salvador', 4),
(5, 'Ferdinand Escamilla Almanza', 'Almanza@gmail.com', '7364-2874', 'San Miguel', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facultad`
--

CREATE TABLE `facultad` (
  `codFacultad` int(11) NOT NULL,
  `nombreFacultad` varchar(50) COLLATE utf8mb4_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `facultad`
--

INSERT INTO `facultad` (`codFacultad`, `nombreFacultad`) VALUES
(1, 'Facultad de Informática'),
(2, 'Facultad de Ciencias Empresariales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripcion`
--

CREATE TABLE `inscripcion` (
  `codInscripcion` int(11) NOT NULL,
  `fecha` datetime DEFAULT NULL,
  `estado` varchar(30) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `codAlumno` int(11) NOT NULL,
  `codMateria` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `inscripcion`
--

INSERT INTO `inscripcion` (`codInscripcion`, `fecha`, `estado`, `codAlumno`, `codMateria`) VALUES
(1, '2020-04-16 12:11:44', 'Activo', 1, 1),
(2, '2020-04-16 12:11:44', 'Activo', 2, 1),
(3, '2020-04-16 12:11:44', 'Activo', 2, 2),
(4, '2020-04-16 12:11:44', 'Activo', 3, 4),
(5, '2020-04-16 12:11:44', 'Activo', 4, 3),
(6, '2020-04-16 12:11:44', 'Activo', 5, 6),
(7, '2020-04-16 12:11:44', 'Activo', 6, 5),
(8, '2020-04-16 12:11:44', 'Activo', 6, 6),
(9, '2020-04-16 12:12:56', 'Activo', 8, 9),
(10, '2020-04-16 12:12:56', 'Activo', 7, 7),
(11, '2020-04-16 12:15:49', 'Activo', 7, 8),
(12, '2020-04-16 12:15:50', 'Activo', 8, 10),
(13, '2020-04-16 12:15:50', 'Activo', 9, 13),
(14, '2020-04-16 12:15:50', 'Activo', 9, 14),
(15, '2020-04-16 12:15:50', 'Activo', 10, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia`
--

CREATE TABLE `materia` (
  `codMateria` int(11) NOT NULL,
  `nombreMateria` varchar(50) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `codCarrera` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `materia`
--

INSERT INTO `materia` (`codMateria`, `nombreMateria`, `codCarrera`) VALUES
(1, 'Programación de Computadoras', 1),
(2, 'Administración de Sistemas', 1),
(3, 'Programación de Sistemas de Computo', 2),
(4, 'Introducción a los Sistemas Informáticos', 2),
(5, 'Telecomunicaciones', 3),
(6, 'Introducción a las Redes de Computadoras', 3),
(7, 'Sistemas Gestores de Bases de Datos', 4),
(8, 'Programación de Bases de Datos', 4),
(9, 'Redes I', 5),
(10, 'Redes II', 5),
(11, 'Mátematica Básica', 6),
(12, 'Estadistica I', 6),
(13, 'Presupuestos', 7),
(14, 'Derecho Mercantil y Laboral', 7),
(15, 'Contabilidad I', 8),
(16, 'Contabilidad de Costos', 8),
(17, 'Sociología General', 9),
(18, 'Administración de Ventas', 9),
(19, 'Economía I', 10),
(20, 'Macroeconomía', 10);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`codAlumno`),
  ADD KEY `FK_carrera` (`codCarrera`);

--
-- Indices de la tabla `carrera`
--
ALTER TABLE `carrera`
  ADD PRIMARY KEY (`codCarrera`),
  ADD KEY `FK_facultad` (`codFacultad`);

--
-- Indices de la tabla `docente`
--
ALTER TABLE `docente`
  ADD PRIMARY KEY (`codDocente`);

--
-- Indices de la tabla `facultad`
--
ALTER TABLE `facultad`
  ADD PRIMARY KEY (`codFacultad`);

--
-- Indices de la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  ADD PRIMARY KEY (`codInscripcion`),
  ADD KEY `FK_alumno` (`codAlumno`),
  ADD KEY `FK_materia` (`codMateria`);

--
-- Indices de la tabla `materia`
--
ALTER TABLE `materia`
  ADD PRIMARY KEY (`codMateria`),
  ADD KEY `FK_carreraM` (`codCarrera`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumno`
--
ALTER TABLE `alumno`
  MODIFY `codAlumno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `carrera`
--
ALTER TABLE `carrera`
  MODIFY `codCarrera` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `docente`
--
ALTER TABLE `docente`
  MODIFY `codDocente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `facultad`
--
ALTER TABLE `facultad`
  MODIFY `codFacultad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  MODIFY `codAlumno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  ADD CONSTRAINT `FK_materia` FOREIGN KEY (`codMateria`) REFERENCES `materia` (`codMateria`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
